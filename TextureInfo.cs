﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTexturePacker
{
    public class TextureInfo
    {
        public Texture2D Texture;
        public string FileName;
        public Vector2 Dimensions;
        public int Area;
        public bool WasAdded = false;
    }
}
