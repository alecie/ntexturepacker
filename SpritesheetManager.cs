﻿using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTexturePacker
{
    public class SpritesheetManager
    {
        public List<Spritesheet> SpritesheetList = new List<Spritesheet>();
        public string SpritesheetFileName = "Spritesheet";

        public SpritesheetManager()
        {

        }

        public Spritesheet GetSpritesheet(string spriteName)
        {
            foreach(var entry in SpritesheetList)
            {
                if(entry.Contains(spriteName))
                {
                    return entry;
                }
            }
            return null;
        }

        public Rectangle? GetRectangle(string id)
        {
            foreach (var entry in SpritesheetList)
            {
                if (entry.Contains(id))
                {
                    return entry.GetRegion(id);
                }
            }
            return null;
        }

        public void AddSpritesheet(Spritesheet spritesheet)
        {
            SpritesheetList.Add(spritesheet);
        }

        public void SaveAllAsPNG(string path)
        {
            int i = 0;
            foreach(var entry in SpritesheetList)
            {
                entry.SaveAsPNG(path + SpritesheetFileName + i + ".png");
                i++;
            }
        }

        public void SaveRegions(string path)
        {
            for(int i = 0; i < SpritesheetList.Count; i++)
            {
                string jsonData = JsonConvert.SerializeObject(SpritesheetList[i]);

                File.WriteAllText(path + i.ToString(), jsonData);
            }

            foreach (var entry in SpritesheetList)
            {
                Color[] data = new Color[entry.Texture.Width * entry.Texture.Height];
                entry.Texture.GetData(data);
                for (int i = 0; i != data.Length; ++i)
                    data[i] = Color.FromNonPremultiplied(data[i].ToVector4());
                entry.Texture.SetData(data);
            }
        }

        public void LoadRegions(string path, int count)
        {
            for (int i = 0; i < count; i++)
            {
                string jsonData = File.ReadAllText(path + i.ToString());

                var result = JsonConvert.DeserializeObject<Spritesheet>(jsonData);
                //result.Texture = 
                SpritesheetList.Add(result);
            }
        }
    }
}
