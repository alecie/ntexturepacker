﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTexturePacker
{
    public class TexturePackerSettings
    {
        public GraphicsDevice GraphicsDevice;
        public int MaxTextureSize = 4096;
        public string FolderPath = "Content//Sprites//PackingTest//";
        public string FileExtension = "*.png";
        public int MaxSpritesheetsCount = 4;
        public bool IncludeSubfolders = true;
        public bool FolderIsSeparateSheet = false;
        public bool CreateTexture = true;
    }
}
