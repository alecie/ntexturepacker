﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTexturePacker
{
    public class TexturePacker
    {
        List<TextureInfo> TextureList = new List<TextureInfo>();

        List<Bin> BinList = new List<Bin>();
        List<Texture2D> SpriteSheetList = new List<Texture2D>();

        Texture2D SpriteSheet;

        Bin binRoot;

        bool couldNotContainAll = false;

        public SpritesheetManager Pack(TexturePackerSettings settings)
        {
            //Console.WriteLine("Starting to pack folder: " + settings.FolderPath);

            SpritesheetManager sm = new SpritesheetManager();

            // Load all textures
            LoadImages(settings);

            // Sort by area
           // SortImages(settings);

            CreateAndAddSheet(settings, ref sm);

            

            while(couldNotContainAll == true)
            {
                binRoot = null;
                SpriteSheet = null;
                couldNotContainAll = false;

                CreateAndAddSheet(settings, ref sm);
            }
            
            return sm;
        }

        void CreateAndAddSheet(TexturePackerSettings settings, ref SpritesheetManager mgr)
        {
            

            SpriteSheet = new Texture2D(settings.GraphicsDevice, settings.MaxTextureSize, settings.MaxTextureSize);
            binRoot = new Bin(new Rectangle(0, 0, settings.MaxTextureSize, settings.MaxTextureSize));

            // pack images
            PackImages(settings);

            if (settings.CreateTexture)
            {
                // save to texture
                SaveToTexture(settings);
            }

            // create spritesheet
            var spritesheet2 = CreateSpritesheet(settings);

            //spritesheet2.Texture
            // premultiply
            

            // add to manager
            mgr.AddSpritesheet(spritesheet2);
        }

        void LoadImages(TexturePackerSettings settings)
        {
            if(settings.IncludeSubfolders)
            {
                LoadFilesRecursive(settings, settings.FolderPath);
            }
            else
            {
                LoadFilesFromFolder(settings, settings.FolderPath);
            }
            

            /*// get folders
            string[] subdirectoryEntries = Directory.GetDirectories(settings.FolderPath);

            foreach (string subdirectory in subdirectoryEntries)
            {
                LoadFilesFromFolder(settings, subdirectory);
            }*/

            
        }

        void LoadFilesRecursive(TexturePackerSettings settings, string path)
        {
            LoadFilesFromFolder(settings, path);

            // get folders
            string[] subdirectoryEntries = Directory.GetDirectories(path);

            foreach (string subdirectory in subdirectoryEntries)
            {
               // LoadFilesFromFolder(settings, subdirectory);

                LoadFilesRecursive(settings, subdirectory);
            }
        }

        void LoadFilesFromFolder(TexturePackerSettings settings, string path)
        {
            string[] files = Directory.GetFiles(path, settings.FileExtension);
            foreach (var entry in files)
            {
                TextureInfo ti = new TextureInfo();
                ti.Texture = LoadTexture(settings, entry);
                ti.Dimensions = new Vector2(ti.Texture.Width, ti.Texture.Height);
                ti.Area = (int)(ti.Dimensions.X * ti.Dimensions.Y);

                string fileName = GetFileNameFromPath(entry);
                ti.FileName = fileName;

                TextureList.Add(ti);

                //Console.WriteLine(fileName);
            }
        }

        void SortImages(TexturePackerSettings settings)
        {
            //PrintArray();
            TextureList.Sort(new AreaComparer());
            //PrintArray();
        }

        void PackImages(TexturePackerSettings settings)
        {
            foreach(var entry in TextureList)
            {
                if(entry.WasAdded == true)
                {
                    continue;
                }

                Bin bin = FindBin(binRoot, entry.Dimensions);
                if(bin != null)
                {
                    //Console.WriteLine("Adding " + entry.FileName + " to bin: " + bin.Rect.X + ", " + bin.Rect.Y + ", " + bin.Rect.Width + ", " + bin.Rect.Height);
                    
                    bin.InsertData(entry);
                    entry.WasAdded = true;
                    //Console.WriteLine("Occupied space: " + bin.OccupiedSpace.X + ", " + bin.OccupiedSpace.Y + ", " + bin.OccupiedSpace.Width + ", " + bin.OccupiedSpace.Height);
                }
                else
                {
                    //Console.WriteLine("Spritesheet can not contain " + entry.FileName);
                    couldNotContainAll = true;
                }
            }
        }

        void SaveToTexture(TexturePackerSettings settings)
        {
            BlitOntoSpritesheet(binRoot);
        }

        Spritesheet CreateSpritesheet(TexturePackerSettings settings)
        {
            Spritesheet spritesheet = new Spritesheet();

            spritesheet.Texture = SpriteSheet;

            AddRegionData(binRoot, ref spritesheet.Regions);

            return spritesheet;
        }

        void AddRegionData(Bin bin, ref Dictionary<string, Rectangle> dictionary)
        {
            if(bin.IsEmpty())
            {
                return;
            }

            dictionary[bin.Data.FileName] = bin.OccupiedSpace;

            foreach (var child in bin.Children)
            {
                

                AddRegionData(child, ref dictionary);
            }
        }

        void BlitOntoSpritesheet(Bin bin)
        {
            if(bin.IsEmpty())
            {
                return;
            }

            Color[] colorData = new Color[bin.Data.Texture.Width * bin.Data.Texture.Height];
            bin.Data.Texture.GetData<Color>(colorData);

            SpriteSheet.SetData<Color>(0, bin.OccupiedSpace, colorData, 0, colorData.Length);

            foreach (var child in bin.Children)
            {
                BlitOntoSpritesheet(child);
            }
        }

        Bin FindBin(Bin bin, Vector2 dimensionToContain)
        {
            if(bin.IsEmpty() && bin.CanContain(dimensionToContain))
            {
                return bin;
            }
            else
            {
                if(bin.Children.Count == 0)
                {
                    return null;
                }

                foreach(var child in bin.Children)
                {
                    Bin result = FindBin(child, dimensionToContain);
                    if(result != null)
                    {
                        return result;
                    }
                }
            }

            return null;
        }

        void PrintArray()
        {
            int i = 0;
            foreach(var entry in TextureList)
            {
                //Console.WriteLine(i + " area: " + entry.Area);
                i++;
            }
        }

        string GetFileNameFromPath(string path)
        {
            string fileName = Path.GetFileName(path);
            int index = fileName.IndexOf('.');
            if (index > 0)
            {
                fileName = fileName.Substring(0, index);
            }
            return fileName;
        }

        Texture2D LoadTexture(TexturePackerSettings settings, string file)
        {
            FileStream fileStream = new FileStream(file, FileMode.Open);
            Texture2D newSprite = Texture2D.FromStream(settings.GraphicsDevice, fileStream);
            fileStream.Dispose();
            return newSprite;
        }
    }
}
