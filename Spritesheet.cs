﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTexturePacker
{
    public class Spritesheet
    {
        [JsonIgnore]
        public Texture2D Texture;
        public Dictionary<string, Rectangle> Regions = new Dictionary<string, Rectangle>();

        public void SaveAsPNG(string path)
        {
            Stream stream = File.Create(path);
            Texture.SaveAsPng(stream, Texture.Width, Texture.Height);
            stream.Dispose();
        }

        public bool Contains(string id)
        {
            return Regions.ContainsKey(id);
        }

        public Rectangle GetRegion(string id)
        {
            return Regions[id];
        }

        public Texture2D GetTexture()
        {
            return Texture;
        }
    }
}
